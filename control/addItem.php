<?php
include('../menu.php');

$mainFile = $_FILES['itemImage'];
$imgPath = "itemImages/";
$moveTo = "/opt/".$imgPath ;
        $mainFileMoveTo = $moveTo.preg_replace("/[^A-Za-z0-9.]/", "", basename($mainFile['name']) );

        if( file_exists( $mainFileMoveTo ) ){
                echo '{ "success": false, "message" : "ERROR: Main file '. $mainFile['name']. ' already exists and can not be overwritten."}';
                exit;
        }
        if(!move_uploaded_file( $mainFile['tmp_name'], $mainFileMoveTo )){
                echo '{ "success": false, "message" : "ERROR: Main file '. $mainFile['name']. ' upload Failed."}';
                exit;
        }

$name = isset( $_POST['itemname'] ) ?  $_POST['itemname']:false;
$category = isset( $_POST['category'] )  ?  $_POST['category']:false; 
$price = isset( $_POST['price'] ) ?  $_POST['price']:false;
$promo_off= isset( $_POST['promooff'] )  ?  $_POST['promooff']:false; 
$description = isset( $_POST['description'] )  ?  $_POST['description']:false; 

Menu::newItem($name, $category, $price, $promo_off, $description, substr($mainFileMoveTo, 5));

echo '{ "success": true }';
?>
