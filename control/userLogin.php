<?php
include '../login.php';
$username = isset( $_REQUEST['userName'] ) ? $_REQUEST['userName'] : false;
$password = isset( $_REQUEST['pwd'] ) ? $_REQUEST['pwd'] : false;
$login = new Login();
$uid = Login::userLogin($username, $password);
if( $uid !== NULL ){
	echo '{ "success": true,
		"uid": '. $uid .',
		"message": "'. $username .' has logined."
	}';
} else {
	echo '{ "success": false,
		"message": "Login Failed"
	}';
}
?>
