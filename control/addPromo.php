<?php
include('../promotion.php');

$name = isset( $_POST['promoname'] ) ?  $_POST['promoname']:false;
$message = isset( $_POST['message'] ) ?  $_POST['message']:false;
$percent_off= isset( $_POST['percentoff'] )  ?  $_POST['percentoff']:false; 
$tier = isset( $_POST['tier'] )  ?  $_POST['tier']:false; 
Promotion::newPromo($name, $tier,  $percent_off, $message, date("Y-m-d H:i:s"));

echo '{ "success": true }';
?>
