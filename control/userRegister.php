<?php
include '../login.php';
include '../user.php';

$username =  isset( $_REQUEST['userName'] ) ? $_REQUEST['userName']: false;
$email = isset( $_REQUEST['email'] ) ? $_REQUEST['email'] : false;
$pwd = isset( $_REQUEST['pwd'] ) ? $_REQUEST['pwd'] : false;
$lastName = isset( $_REQUEST['lastName'] ) ? $_REQUEST['lastName'] : false;
$firstName = isset( $_REQUEST['firstName'] ) ? $_REQUEST['firstName'] : false;
$pwd2 = isset( $_REQUEST['pwd2'] ) ? $_REQUEST['pwd2'] : false;
$phone = isset( $_REQUEST['phone'] ) ? $_REQUEST['phone'] : false;

$login = new Login();
$user = new User();
$uid = Login::newLogin( $username, $pwd, $pwd2 );
if( $uid === NULL ){
	echo '{ "success": false,
		"message": "Something wrong!"
	}';
} else {
	User::createUser( $uid, $firstName, $lastName, $email, 1, 1, date("Y-m-d H:i:s") );
	echo '{ "success": true,
		"message": "User Registered."
	}';
}
?>
