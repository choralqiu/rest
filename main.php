<?php
include 'user.php';
include 'order.php';
include 'menu.php';
include 'promotion.php';
include 'message.php';
include 'summary.php';
//include 'sampleitems.php';
//include 'sampleOrder.php';


$uid = isset( $_POST['uid'] ) ? $_POST['uid'] : false;
$username = isset( $_POST['username'] ) ? $_POST['username'] : false;

if( $uid ){
	$user = User::loadUser( $uid );
	$orders = Order::getAllOrders( $username );
	if( $username == "admin" ){
		$items = Menu::getAvailableItems( false );
		$promos = Promotion::getActivePromotion( false );
		$totalSummary = Summary::getTotalSummary();
	} else {
		$items = Menu::getAvailableItems( true );
		$promotions = Message::getMessageByTier( $user['tier']);
	}
}
if( !$user) {
	echo "can not load user";
	exit;
}
function renderOrder( $item ){

	if( $item['order_status'] == "open" ){
		$status = "<span class=\"status open\"><strong>接单</strong></span>";
		$status.= "<span class=\"status processing\"><a href=\"#\" onclick=\"updateOrder( {$item['id']}, 'processing')\">制作中</a></span>";
		$status.= "<span class=\"status completed\"><a href=\"#\" onclick=\"updateOrder( {$item['id']}, 'completed')\">完成</a></span>";
	}
	if(  $item['order_status'] == "processing"  ){
		$status= "<span class=\"status open\">接单</span><span class=\"status processing\"><strong>制作中</strong></span>";
		$status.= "<span class=\"status completed\"><a href=\"#\" onclick=\"updateOrder( {$item['id']}, 'completed')\">完成</a></span>";
	}
	if(  $item['order_status'] == "completed" ){
		$status= "<span class=\"status open\">接单</span><span class=\"status processing\">制作中</span><span class=\"status complete\"><strong>完成</strong></span>";
	}
	echo $status;
}

function renderItem( $item ){
	if( $item['in_stock'] == 1 ){
		$status = "<span class=\"status enabled\"><strong>上架</strong></span>";
		$status.= "<span class=\"status disabled\"><a href=\"#\" onclick=\"updateMenu( {$item['id']}, 0 )\">下架</a></span>";
	} else {
		$status= "<span class=\"status enabled\"><a href=\"#\" onclick=\"updateMenu( {$item['id']}, 1 )\">上架</a></span>";
		$status .= "<span class=\"status disabled\"><strong>下架</strong></span>";

	}
	echo $status;
}

?>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title></title>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/amcharts.js" type="text/javascript"></script>
  <script src="js/serial.js" type="text/javascript"></script>
  <script src="js/jquery-1.12.4.js"></script>
  <script src="js/jquery-ui.js"></script>

  <script>
	var uid = <?=$uid?>;
	var username = "<?=$username?>";
<?php 
	if( $username == "admin" ){
?>
	/////////////////////////////////
	////////////////////////////////
	var chart;
	var chartData = <?php echo json_encode(Summary::getCountSummary());?>;
	var chartData2 = <?php echo json_encode(Summary::predictionByCategory());?>;
	AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;//数据提供来源
                
                chart.categoryField = "name";//X轴字段
                chart.startDuration = 1;//
                chart.plotAreaBorderColor = "#DADADA";//绘图区域边框颜色
                chart.plotAreaBorderAlpha = 1;//绘图区域透明度
                chart.color = "#FFFFFF";//图标颜色
                // this single line makes the chart a bar chart
//                chart.rotate = true;

                // AXES
                // Category
                var categoryAxis = chart.categoryAxis;//图表的X轴
                categoryAxis.gridPosition = "start";//网格位置
                categoryAxis.gridAlpha = 0.1;
                categoryAxis.axisAlpha = 0;

                // Value
                var valueAxis = new AmCharts.ValueAxis();//图标的Y轴
                valueAxis.axisAlpha = 0;
                valueAxis.gridAlpha = 0.1;
                valueAxis.position = "top";
                chart.addValueAxis(valueAxis);//添加Y轴

                // GRAPHS
                // first graph
                var graph1 = new AmCharts.AmGraph();
                graph1.type = "column";
                graph1.title = "count";
                graph1.valueField = "count";
                graph1.balloonText = "Total Sold:[[value]]";
                graph1.lineAlpha = 0;
                graph1.fillColors = "#ADD981";
                graph1.fillAlphas = 1;
                chart.addGraph(graph1);//添加图形(柱状图)

                chart.creditsPosition = "top-right";

                // WRITE
                chart.write("chartdiv");


                // SERIAL CHART
                chart2 = new AmCharts.AmSerialChart();
                chart2.dataProvider = chartData2;
                chart2.categoryField = "category";
                chart2.startDuration = 1;
                chart2.plotAreaBorderColor = "#DADADA";
                chart2.plotAreaBorderAlpha = 1;
                chart2.color = "#FFFFFF";

                // AXES
                // Category
                var categoryAxis = chart2.categoryAxis;
                categoryAxis.gridPosition = "start";
                categoryAxis.gridAlpha = 0.1;
                categoryAxis.axisAlpha = 0;

                // Value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                valueAxis.gridAlpha = 0.1;
                valueAxis.position = "top";
                chart2.addValueAxis(valueAxis);

                // GRAPHS
                // second graph
                var graph2 = new AmCharts.AmGraph();
                graph2.type = "column";
                graph2.title = "number";
                graph2.valueField = "number";
                graph2.balloonText = "Amount to Prepare:[[value]]";
                graph2.lineAlpha = 0;
                graph2.fillColors = "#81acd9";
                graph2.fillAlphas = 1;
                chart2.addGraph(graph2);

                chart2.creditsPosition = "top-right";

                // WRITE
                chart2.write("chartdiv2");




            });

	/* End of Graphing */
<?php }
?>
	$(document).ready(function() {
	$("#addItem").submit( function( event) {
		event.preventDefault();
		var formData = new FormData($(this)[0]);
		console.log( formData);
		$.ajax({
			url: "control/additem.php",
			type: 'post',
			data: formData,
			async: false,
	    cache: false,
	    contentType: false,
    		processData: false,
		        success:function( result ){
				 var response = JSON.parse( result );
				 if( response.success ){
					$("#successMessage").html( "item added. please refresh the page to see new items in manage items." );
					$("#successMessage").show();
					setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
				} else {
					$("#errorMessage").html( response.message );
					$("#errorMessage").show();
					setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
		                }
			}
		});
		});
		$( function() {
 			$( "#tabs" ).tabs();
		} );
	});
	function search(){
		var input, filter, table, tr, td, i;
		input = document.getElementById("searchInput");
		filter = input.value.toLowerCase();
		table = document.getElementById("customerorder");
		tr = table.getElementsByTagName("tr");
 		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[1];
			if (td) {
				if (td.innerHTML.toLowerCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			} 
  		}
	}
	function clearSearchItem(){
		document.getElementById("searchInput").value = '';
		search();
	}
	function deletePromo( id ){
		$.ajax({
			url: "control/deletePromo.php",
			type: 'POST',
			data: {
				id:id
			},
		        success:function( result ){
				 var response = JSON.parse( result );
				 if( response.success ){
					$("#successMessage").html( "Promo removed" );
					$("#successMessage").show();
					$("#promo"+id).remove();
					setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
				} else {
					$("#errorMessage").html( response.message );
					$("#errorMessage").show();
					setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
		                }
			}
		});
	}
	function addPromo(){
		var promoname = $("#promoname").val(),
		    message =  $("#message").val(),
		    tier =  $("#promotier").val(),
		    percentoff = $("#percentoff").val();
		$.ajax({
			url: "control/addPromo.php",
			type: 'POST',
			data: {
				promoname: promoname,
				message: message,
				tier: tier,
				percentoff: percentoff
			},
		        success:function( result ){
				 var response = JSON.parse( result );
				 if( response.success ){
					$("#successMessage").html( "Promo Added. Please refresh the page to see new promos in Manage Promos." );
					$("#successMessage").show();
					setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
				} else {
					$("#errorMessage").html( response.message );
					$("#errorMessage").show();
					setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
		                }
			}
		});
	}
	function addPoints(){
		var username = $("#usernameAddValue").val(),
		    amountAddValue =  $("#amountAddValue").val();

		$.ajax({
			url: "control/addPoints.php",
			type: 'POST',
			data: {
				username: username,
				amount: amountAddValue
			},
		        success:function( result ){
				 var response = JSON.parse( result );
				 if( response.success ){
					$("#successMessage").html( "Value Added" );
					$("#successMessage").show();
					setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
				} else {
					$("#errorMessage").html( response.message );
					$("#errorMessage").show();
					setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
		                }
			}
		});
	}

	function updateMenu( id, status ){
		 $.ajax({
                        url: "control/updateMenu.php",
                        type: 'POST',
                        data: {
                                id: id,
				status: status
			},
			success:function( result ){
				var response = JSON.parse( result );
				if( response.success ){
					var html="";
					if( status == 1 ){
						html = "<span class=\"status enabled\"><strong>上架</strong></span>";
				                html += "<span class=\"status disabled\"><a href=\"#\" onclick=\"updateMenu( "+id +", 0 )\">下架</a></span>";
					}
					if( status == 0 ){
						html = "<span class=\"status enabled\"><a href=\"#\" onclick=\"updateMenu( "+ id +", 1 )\">上架</a></span>";
                				html += "<span class=\"status disabled\"><strong>下架</strong></span>";
					}
					$('#itemStatus'+id).html(html);
				 	$("#successMessage").html( "Item is updated." );
                                        $("#successMessage").show();
                                        setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
                                } else {
                                        $("#errorMessage").html( response.message );
                                        $("#errorMessage").show();
                                        setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
                                }
			}
		});

	}

	function addComment( id ){
		 $.ajax({
                        url: "control/addComment.php",
                        type: 'POST',
                        data: {
                                id: id,
				comment: $('#orderComment'+id).val()
			},
			success:function( result ){
				var response = JSON.parse( result );
				if( response.success ){
					$('#orderCommentTd'+id).html($('#orderComment'+id).val());
				 	$("#successMessage").html( "Comment is aadded!" );
                                        $("#successMessage").show();
                                        setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
                                } else {
                                        $("#errorMessage").html( response.message );
                                        $("#errorMessage").show();
                                        setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
                                }
			}
		});

	}
	function updateOrder( id, status ){
		 $.ajax({
                        url: "control/updateOrder.php",
                        type: 'POST',
                        data: {
                                id: id,
				status: status
			},
			success:function( result ){
				var response = JSON.parse( result );制作中
				if( response.success ){
					var html="";
					if( status == "processing" ){
						html= "<span class=\"status open\">接单</span><span class=\"status processing\"><strong>制作中</strong></span>";
						html += "<span class=\"status completed\"><a href=\"#\" onclick=\"updateOrder( " +id +", 'completed')\">完成</a></span>";
					}
					if( status == "completed" ){
						html = "<span class=\"status open\">接单</span><span class=\"status processing\">制作中</span><span class=\"status complete\"><strong>完成</strong></span>";
					}
					$('#orderStatus'+id).html(html);
				 	$("#successMessage").html( "Order is Updated!" );
                                        $("#successMessage").show();
                                        setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
                                } else {
                                        $("#errorMessage").html( response.message );
                                        $("#errorMessage").show();
                                        setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
                                }
			}
		});

	}
	function update(){
		var uid = $("#uid").val(),
		    firstname =  $("#firstname").val(),
		    lastname =  $("#lastname").val(),
		    email = $("#email").val(),
		    phone = $("#phone").val(),
		    password = $("#loginPwd").val(),
		    pwd = $('#registerpwd').val(),
         	   pwd2 = $('#registerpwd2').val();
	        if( pwd !== pwd2 ){
        	        $("#errorMessage").html( "Passwords doesn't match!" );
                	$("#errorMessage").show();
			setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
	                return;
        	}

		$.ajax({
			url: "control/userUpdate.php",
			type: 'POST',
			data: {
				uid: uid,
				password: password,
				firstname: firstname,
				lastname: lastname,
				phone: phone,
				email: email,
				newpwd: pwd,
				newpwd2: pwd2
			},
		        success:function( result ){
				 var response = JSON.parse( result );
				 if( response.success ){
					$("#successMessage").html( "User information is updated!" );
					$("#successMessage").show();
					setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
				} else {
					$("#errorMessage").html( response.message );
					$("#errorMessage").show();
					setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
		                }
			}
		});
	}
	function calculate(){
		var items = <?php echo json_encode( $items );?>;
		var total = 0;
		var order = [];
		items.forEach( function(item){
			var qty = $("#item-"+item.id).val();
			total += item.price * qty;
			if( qty > 0 ){
				order.push({ id: item.id, qty: qty});
			}
		});
		$("#ordertotal").html( "$"+total );
		return { total: total, order: order };
	}

	function placeOrder(){
		var data = calculate();
		$.ajax({
                        url: "control/placeOrder.php",
                        type: 'POST',
                        data: {
                                username: username,
                        	order: data
			},
                        success:function( result ){
       				 var response = JSON.parse( result );
				if( response.success ){
					$("#successMessage").html( "Your order is placed!" );
					$("#successMessage").show();
					setTimeout(function(){ $("#successMessage").hide( 2000); }, 5000);
				} else {
					$("#errorMessage").html( response.message );
					$("#errorMessage").show();
					setTimeout(function(){ $("#errorMessage").hide( 2000); }, 5000);
		                }
                 }
                });
	}
	function searchOrder(){
		var searchString = $('#searchOrder').val();
		var orders = $('.orderItem');
		if( searchString == "" ) {
			clearSearch();
			return;
		}
		orders.each( function(){
			var username = $(this).find('td.orderUsername').html();
			if( username.toLowerCase().indexOf( searchString.toLowerCase() ) == -1 ){
				$(this).hide();
			} else {
				$(this).show();
			}
			
		});

	}
	function clearSearch(){
		var orders = $('.orderItem');
		orders.each( function(){
			 $(this).show();
		});
	}
  </script>
<style>
/*a:active {color: #FFD306}*/
/*li:active a{
color:#FFFF37;
}*/
.zoom {
	border: thick solid #000000;
    transition: transform .2s; /* Animation */
    width: 50px;
    height: 50px;
    margin: 0 auto;
}
.zoom:hover {
    transform: scale(5); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
.status{
	width:100px;
	display:inline-block;
	padding-right:5px;
}
.formfield{
	display: block;
	padding-bottom: 5px;
	width:600px;
}
.formfield>input{
	display:inline-block !important;
}
.formfield>label{
	width: 200px;
	text-align: right !important;
        display:inline-block !important;
	padding-right:10px;
}
form{
	width:600px;
}
input{
	width: 350px;
}
input[type="text"]:disabled {
    background: #dddddd;
	border: 0px;
}
hr{
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
input.itemqty{
	padding: 5px;
	width:45px;
}
#placeorder{
width:700px;
}
#tabs{
background:transparent;<!--±³¾°Í¸Ã÷--> 
¡¡color:#FAFAD2;<!--×ÖÌåÑÕÉ«--> 
¡¡background-color:rgba(255,255,255,0.15)<!--×îºóÒ»¸ö²ÎÊýÉèÖÃÍ¸Ã÷¶È£¬Ç°ÃæÈý¸öÊÇRGBÑÕÉ«Öµ--> 
}
body{
	margin: 0 auto;
	background-image: url(./5.png) 

}
</style>
</head>
<body>
 		<div class="messageHolder" style="align:center;margin:0 auto;style:100%">
			<div class="message" id="errorMessage"></div>
			<div class="message" id="successMessage"></div></div>

<div id="tabs" style="">
  <ul>
<?php
if( $username == "admin" ){
?>
    <li><a href="#tabs-4">用户充值</a></li>
    <li><a href="#tabs-7">添加菜品</a></li>
    <li><a href="#tabs-5">菜品管理</a></li>
    <li><a href="#tabs-8">添加优惠活动</a></li>
    <li><a href="#tabs-9">优惠管理</a></li>
    <li><a href="#tabs-6">订单管理</a></li>
    <li><a href="#tabs-10">销售报表</a></li>
    <li><a href="#tabs-12">销售预测</a></li>
<?php }else{?>
    <li><a href="#tabs-3">点菜下单</a></li>
    <li><a href="#tabs-1">用户信息</a></li>
    <li><a href="#tabs-2">历史订单</a></li>
    
<?php }?>
</ul>
<?php
if( $username == "admin" ){
?>
 <div id="tabs-4" style="">
        <div style="width:600px">
	<form id='addValue'onsubmit="return false;">
		<input type="hidden" id="uid" required value="<?=$uid?>" disabled>
		<span class="formfield">
			<label style="color:#FAFAD2"> 用户名:</label>
			<input type="text" placeholder="Username" id="usernameAddValue" required></span>
		<span class="formfield">
			<label style="color:#FAFAD2">金额:</label>
			<input type="number" placeholder="Points(number)" id="amountAddValue" required></span>
	         <button onclick="addPoints()">充值</button>
	</form>
	</div>
  </div>
  <div id="tabs-7">
	<div style="width:600px">
		<form id='addItem' action="control/addItem.php" method="post">
	<span class="formfield">
		<label style="color:#FAFAD2"> 名称:</label>
		<input type="text" placeholder="Item Name" id="itemname" name="itemname" required></span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 品类:</label>
		<input type="text" placeholder="Category" id="category" name="category" required</span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 价格:</label>
		<input type="number" placeholder="Price" id="price" required min="0" name="price"></span>
	 <span class="formfield">
                <label style="color:#FAFAD2"> 优惠折扣:</label>
		<input type="number" id="promooff" min="0" required name="promooff"></span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 菜品描述:</label>
		<input type="textfield" placeholder="description" id="description" required name="description"></span>
	<span class="formfield">
                <label style="color:#FAFAD2"> 菜品图片:</label>
		<input type="file" id="itemImage" name="itemImage" accept="image/*"></span>

	 <button>添加菜品</button>
        </form>
	</div>
</div>
<div id="tabs-5">
  	<?php
		if( count($items) > 0 ){ ?>
		<div style="width:800px"><table id="placeorder" style="width:100%;">
			<tr>
			    <th>菜品名称</th>
			    <th>品类</th>
			    <th>价格</th>
			    <th>上架管理</th>
			</tr>

	<?php foreach ($items as $key=>$item): ?>
		<tr>
		     <td style="text-align:center;"><span title="<?php echo $item['description']?>"><?php echo $item['name'] ?></span></td>
		     <td style="text-align:center;"><?php echo $item['category'] ?></td>
		     <td style="text-align:center;"class="currency"><?php echo "$".$item['price'] ?></td>
		     <td id="itemStatus<?php echo $item['id']?>"><?php renderItem($item);?></td>
		</tr>
	<?php endforeach; ?>
	</table></div>
	<?php } else {?>
		<p style="color:#FAFAD2">Store is not open yet. We are currently trying to stock our inventory. </p>
	<?php }?>


  </div>
 <div id="tabs-8">
	<div style="width:600px">
		<form id='addPromo' onsubmit="return false;">
	<span class="formfield">
		<label style="color:#FAFAD2"> 名称:</label>
		<input type="text" placeholder="Promo Name" id="promoname" required></span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 消息:</label>
		<input type="text" placeholder="Message" id="message" required</span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 等级:</label>
		<input type="number" placeholder="Tier" id="promotier" required min="0"></span>
	 <span class="formfield">
                <label style="color:#FAFAD2"> 优惠折扣:</label>
		<input type="number" id="percentoff" min="0" required></span>
	 <button onclick="addPromo()">添加优惠</button>
        </form>
	</div>
</div>

<div id="tabs-9">
  	<?php
		if( count($promos) > 0 ){ ?>
		<div style="width:800px"><table id="placeorder" style="width:100%;">
			<tr>
			    <th>优惠名称</th>
			    <th>优惠消息</th>
			    <th>等级</th>
			    <th>优惠折扣</th>
			    <th>删除</th>
			</tr>

	<?php foreach ($promos as $key=>$item): ?>
		<tr id="promo<?php echo $item['id']?>"> 
		     <td style="text-align:center;"><?php echo $item['name'] ?></span></td>
		     <td style="text-align:center;"><?php echo $item['message_text'] ?></td>
		     <td style="text-align:center;"><?php echo $item['tier'] ?></td>
		     <td style="text-align:center;"class="currency"><?php echo $item['percent_off']."%" ?></td>
		     <td ><a hrefrenderItem($item['id');?><a href="#" onclick="deletePromo(<?php echo $item['id']?>);">Delete</a></td>
		</tr>
	<?php endforeach; ?>
	</table></div>
	<?php } else {?>
		<p style="color:#FAFAD2">暂时没有优惠活动!</p>
	<?php }?>

  </div>
<div id="tabs-6">
	<input type="text" id="searchOrder" placeholder="根据用户名查找"><button onclick="searchOrder()">查询</button><button onclick="clearSearch()">重置</button>
	
	 <?php
		if( count($orders) > 0 ){ ?>
			<table>
				<tr>
				    <th>订单编号</th>
				    <th>用户名</th>
				     <th>金额</th>
				    <th>订单状态</th>
				    <th>评价</th>
				</tr>

	<?php foreach ($orders as $item): ?>
			<tr class="orderItem">
			    <td><?php echo $item['id'] ?></td>
			    <td class="orderUsername"><?php echo $item['username'] ?></td>
			    <td class="currency"><?php echo "$".$item['total'] ?></td>
			    <td id="orderStatus<?php echo $item['id']?>"><?php renderorder($item);?></td>
			     <td ><?php echo $item['comment']?></td>
			</tr>
	<?php endforeach; ?>
		</table>
	<?php } else {?>
			<p style="color:#FAFAD2"> 暂时没有任何订单需要管理!</p>
		<?php }?>
</div>
  <div id="tabs-10">
	<table width="50%">
		<thead>
		<td>菜品广度</td>
		<td>营业额</td>
		<td>订单量</td>
		<td>用户广度</td>
		</thead>
		<tbody>
		<tr>
		<td><?php echo $totalSummary['menu_spread'];?></td>
		<td><?php echo $totalSummary['revenue'];?></td>
		<td><?php echo $totalSummary['order_count'];?></td>
		<td><?php echo $totalSummary['distinct_user'];?></td>
		</tr>
		</tbody>
	</table>
        <div id="chartdiv" style="width:50%; height:600px;"></div>

</div>
  <div id="tabs-12">
        <div id="chartdiv2" style="width:50%; height:600px;"></div>

</div>
<?php }else{
?>

  <div id="tabs-1">
	<div style="width:600px;">
		<form id='userInfo'onsubmit="return false;">
	<input type="hidden" id="uid" required value="<?=$uid?>" disabled>
	<span class="formfield">
		<label style="color:#FAFAD2"> 用户名:</label>
		<input type="text" placeholder="Username" id="username" required value="<?=$username?>" disabled></span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 姓氏:</label>
		<input type="text" placeholder="First Name" id="firstname" required value="<?=$user['first_name']?>"></span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 名字:</label>
		<input type="text" placeholder="Last Name" id="lastname" required value="<?=$user['last_name']?>"></span>
	 <span class="formfield">
                <label style="color:#FAFAD2"> 电子邮箱:</label>
		<input type="email" placeholder="Email" id="email" ></span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 手机号:</label>
		<input type="text" placeholder="Cellphone" id="cellphone" required value="<?=$user['phone']?>"></span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 等级:</label>
		<input type="text" placeholder="Tier" id="tier" required value="<?=$user['tier']?>" disabled></span>
	<span class="formfield">
		<label style="color:#FAFAD2"> 余额:</label>
		<input type="text" placeholder="Points" id="points" required value="<?=$user['points']?>" disabled></span>


	<hr>
	<span class="formfield">
		<label style="color:#FAFAD2"> 当前的密码:</label>
		<input type="password" placeholder="********" id="loginPwd"></span>
	<span class="formfield">
                <label style="color:#FAFAD2"> 新密码:</label>
		<input type="password" placeholder="Password" id="registerpwd"></span>
 	<span class="formfield">
                <label style="color:#FAFAD2"> 确认新密码:</label>
		<input type="password" placeholder="Re Password" id="registerpwd2"></span>
	 <button onclick="update()">更新信息</button>
	</form>
	</div>
	<div style="padding-top:30px ;">
		<p style="color:#FAFAD2">您当前的优惠:</p>
		<?php if( count( $promotions ) == 0 ){
			echo "<p style=\"color:#FAFAD2\">当前没有优惠活动。</p>";
		}else{
			echo "<ul>";
			foreach( $promotions as $promotion ){
				echo "<li style=\"padding-bottom:10px;color:red;\">$promotion</li>";
			}
		}	echo "</ul>";
		?>

	</div>
  </div>
  <div id="tabs-2">
	<?php
		if( count($orders) > 0 ){ ?>
		<table>
			<tr>
			    <th>订单编号</th>
			     <th>金额</th>
			    <th>订单状态</th>
			    <th>评价</th>
			</tr>

	<?php foreach ($orders as $item): ?>
		<tr>
		     <td><?php echo $item['id'] ?></td>
		     <td style="text-align:center;"class="currency"><?php echo "$".$item['total'] ?></td>
		     <td><?php echo $item['order_status'] ?></td>
		     <td id="orderCommentTd<?php echo $item['id']?>">
			<?php if( strlen( $item['comment'] ))
				echo $item['comment'] ;
			else
			?>
				<input type="text" id="orderComment<?php echo $item['id']?>"><button onclick="addComment(<?php echo $item['id']?>)">添加评价</button>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
	<?php } else {?>
		<p style="color:#FAFAD2"> 您目前没有任何订单！ </p>
	<?php }?>


 </div>
  <div id="tabs-3">
	<div style="padding-left:40px;display:block;">
		<input type="text" id="searchInput" onkeyup="search()" placeholder="Search for names..">
		<button type="button" onclick="clearSearchItem();">Clear</button>
	</div>

 	<?php
		if( count($items) > 0 ){ ?>
		<div style="width:800px;padding-left:40px;"><table id="customerorder" style="width:100%;">
			<tr>
			    <th width="50"> </th>
			    <th>菜品名称</th>
			     <th>价格</th>
			    <th width="50">数量</th>
			</tr>

	<?php foreach ($items as $key=>$item): ?>
		<tr>
		     <td width="50">
			<?php if( strlen( $item['pic_link']) && file_exists("/opt/".$item['pic_link']) ){ 
				echo "<img class=\"zoom\" src=\"{$item['pic_link']}\" >";
			}
			?>
			</td> 
		     <td style="text-align:center;"><span title="<?php echo $item['description']?>"><?php echo $item['name'] ?></td>
		     <td style="text-align:center;" class="currency"><?php echo "$".$item['price'] ?></td>
		     <td><input type="number" class="itemqty" placeholder="Username" id="item-<?php echo $item['id']?>" required min="0" value="0"></td>
		</tr>
	<?php endforeach; ?>
		 <tr>
                     <td class="currency"colspan="2"><strong>总计:</strong</td>
                     <td class="currency" id="ordertotal"> $0 </td>
                     <td class="currency" >&nbsp;</td>
               </tr>
	</table>
		<div style="width:100%;padding-top:20px;text-align:right;"><button onclick="calculate()" style="margin-right:20px">计算金额</button>
		<button onclick="placeOrder()">下单</button></div></div>
	<?php } else { ?>
		<p style="color:#FAFAD2">Store is not open yet. We are currently trying to stock our inventory. </p>
	<?php }?>
	<hr>
<span style="display:block;color:white;size:20px;font-weight:bold" >	:: 热销菜品 ::</span> 
	<?php
		$popularItems = Menu::getPopularItems();
		foreach ($popularItems as $popularItem ){
	?>	
		<div class="popular" style="display:inline-block;color: white;text-align:center;padding:10px">
			<div style="width:200px;"><img src="<?php echo $popularItem['pic_link'];?>" width="200"></div>
				<span  style="display:block"><?php echo $popularItem['name'];?></span>
				<span  style="display:block">$<?php echo $popularItem['price'];?></span>
		</div>
        <?php }?>

 </div>
<?php }
?>
</div>


</body>
</html>
