<?php 
class HoltWinters { 

    public static function predict($category, $alpha=0.1, $beta=0.1, $gamma=0.1, $seasonality=10080 ){
        $historicalData = self::fetchHistoricalBaselines($category);
        $preLevel = $historicalData['level'];
        $preSlope = $historicalData['slope'];
        $preSeasonality = $historicalData['seasonality'];

        $currentLevel = self::fetchCurrentLevel($category);
        $level = self::calcLevel($alpha, $currentLevel, $preLevel, $preSlope, $preSeasonality);
        $slope = self::calcSlope($beta, $level, $preLevel, $preSlope);
        $seasonality = self::calcSeasonality($gamma, $currentLevel, $level, $preLevel, $preSlope, $preSeasonality);
        self::storeNewBaselines($category, $level, $slope, $seasonality);
        return $level+$slope+$seasonality;
    }

    private function fetchHistoricalBaselines($category){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $seasonalityQuery = "SELECT sum(oi.item_count) AS count FROM diner.menu AS m JOIN diner.order_item AS oi ON m.id=oi.item_id JOIN diner.order AS o ON oi.order_id=o.id WHERE o.create_time>DATE_SUB(now(), INTERVAL 10080 MINUTE) AND o.create_time<DATE_SUB(now(), INTERVAL 8640 MINUTE) AND category='".$category." GROUP BY m.id;";
        $queryResult = $conn->query($seasonalityQuery);
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $summary['seasonality'] = $row['count'];//之前的se
            }
        }

        $query = "SELECT level, slope, seasonality FROM diner.baseline WHERE category='".$category."';";
        $queryResult = $conn->query($query);
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $summary['level'] = $row['level'];//之前的数据
                $summary['slope'] = $row['slope'];
                return $summary;
            }
        } else {
            $summary['level'] = 0;//如果没有都为0
            $summary['slope'] = 0;
            $summary['seasonality'] = 0;
            return $summary;
        }

    }
    
    private function storeNewBaselines($category, $level, $slope, $seasonality){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "INSERT INTO diner.baseline (category, level, slope, seasonality) VALUES ('".$category."', ".$level.",".$slope.",".$seasonality.") ON DUPLICATE KEY UPDATE level=".$level.", slope=".$slope.", seasonality=".$seasonality.";";
        $conn->query($query);//存储算完后的数据
    }

    private function fetchCurrentLevel($category){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "SELECT sum(item_count) AS count FROM diner.order AS o JOIN diner.order_item AS oi ON o.id=oi.order_id JOIN diner.menu AS m ON oi.item_id = m.id WHERE create_time>DATE_SUB(now(), INTERVAL 1440 MINUTE) AND category='".$category."' group by category;";
        $queryResult = $conn->query($query);
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                return $row['count'];//计算当前的数据  
            } 
        }
    }

    private function calcLevel($alpha, $currentLevel, $preLevel, $preSlope, $preSeasonality){
        $tmp1 = $alpha*($currentLevel - $preSeasonality);
        $tmp2 = (1-$alpha)*($preLevel+$preSlope);
        return $tmp1+$tmp2;
    }

    private function calcSlope($beta, $level, $preLevel, $preSlope){
        $tmp1 = $beta*($level-$preLevel);
        $tmp2 = (1-$beta)*$preSlope;
        return $tmp1+$tmp2;
    }

    private function calcSeasonality($gamma, $currentLevel, $level, $preLevel, $preSlope, $preSeasonality){
        $tmp1 = $gamma*($currentLevel - $preLevel - $preSlope);//现在的se
        $tmp2 = (1-$gamma)*$preSeasonality;//之前的se
        return $tmp1+$tmp2;
        
    }

};
?> 
