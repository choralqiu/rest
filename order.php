<?php 
include 'points.php';

class Order { 

    public static function newOrder($username, $orderItems=array(), $total){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "INSERT INTO diner.order (username, total, order_status ) VALUES (\"$username\", \"$total\", \"open\")";
        $result = $conn->query($query);
        if ($result === TRUE) {
            $orderId = $conn->insert_id;
        } else {
            return -1;
        }
        foreach($orderItems as $orderItem){
            $itemQuery = "INSERT INTO diner.order_item (order_id, item_id, item_count) VALUES (\"$orderId\", ".$orderItem["id"].", ".$orderItem["qty"].")";
            $result = $conn->query($itemQuery);
        }
        Points::reduceFund($username, $total);
    }

    public static function updateOrderStatus( $orderId, $status ){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "UPDATE diner.order SET order_status=\"$status\" WHERE id=\"$orderId\"  ";
        $result = $conn->query($query);
    }

    public static function addComment($orderId, $comment){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "UPDATE diner.order set comment=\"$comment\" WHERE id=\"$orderId\" ";
        $result = $conn->query($query);
    }

    public static function getAllOrders( $username ){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
    	$query = "";
        if( $username == 'admin' ){
    		//Get everything
            $query="SELECT * FROM diner.order";
            
    	} else {
    		//add where clause
            $query="SELECT * FROM diner.order WHERE username = \"$username\" ";
    	}
        $queryResult = $conn->query($query);
        $results = array();
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $result['id'] = $row['id'];
                $result['username'] = $row['username'];
                $result['order_status'] = $row['order_status'];
                $result['discount'] = $row['discount'];
                $result['total'] = $row['total'];
                $result['comment'] = $row['comment'];
                $results[] = $result;
            }
        }
        return $results;
    }
};
?> 
