<?php 
class Points { 

    public static function addFund($user, $amount){
    	$conn = new mysqli("localhost", "root", "mysql", "diner");
    	
        $pointsQuery = "SELECT points.points, promotion.percent_off FROM login JOIN user ON login.id=user.id JOIN points ON login.username=points.username JOIN promotion ON user.tier=promotion.tier WHERE login.username=\"$user\" ";
        $queryResult = $conn->query($pointsQuery);
        $points=0;
        $percent_off=0;
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $points = $row['points'];
                $percent_off = $row['percent_off'];
            }
        }
        $newPoints = $points + ($amount/(100-$percent_off)*100);

        $query = "UPDATE diner.points SET points=\"$newPoints\" WHERE username=\"$user\"";
        $conn->query($query);
        self::updateTierBasedOnPoints($user);
    }

    public static function reduceFund($user, $amount){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "UPDATE diner.points SET points=points-$amount WHERE username=\"$user\" ";
        $conn->query($query);
        self::updateTierBasedOnPoints($user);
    }

    public static function updateTierBasedOnPoints( $user ){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $pointsQuery = "SELECT points FROM points WHERE username=\"$user\" ";
        $queryResult = $conn->query($pointsQuery);
        $points=0;
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $points = $row['points'];
            }
        }
        $orderQuery = "SELECT sum(total) as total FROM diner.order WHERE username=\"$user\" ";
        $queryResult1 = $conn->query($orderQuery);
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult1) ){
                $points += $row['total'];
            }
        }
        $tier = 1;
        if($points > 100){
            $tier++;
        }
        if($points > 200){
            $tier++;
        }
        if($points > 300){
            $tier++;
        }
        if($points > 400){
            $tier++;
        }
        $someQuery = "SELECT id from diner.login WHERE username=\"$user\" ";
        $queryResult2 = $conn->query($someQuery);
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult2) ){
                $id = $row['id'];
            }
        }
        $tierQuery = "UPDATE diner.user SET tier=\"$tier\" WHERE id = \"$id\"  ";
        $conn->query($tierQuery);
    }
};
?> 
