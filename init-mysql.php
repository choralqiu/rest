<?php
    // Create a new database for diner
    $conn = new mysqli("localhost", "root", "mysql");
    $sql = "CREATE DATABASE diner";
    if ($conn->query($sql) === TRUE) {
        echo "Database created successfully\n";
    } else {
        echo "Error creating database: " . $conn->error;
    }
    $conn->close();
    unset($conn);

    // Connecting directly to diner
    // Don't do this in prod
    $conn = new mysqli("localhost", "root", "mysql", "diner");
    
    // User Information
    // Tier is for the promotion level
    $user = "CREATE TABLE diner.user (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        first_name VARCHAR(30) NOT NULL,
        last_name VARCHAR(30) NOT NULL,
        email VARCHAR(50),
        phone VARCHAR(50),
        tier INT(6),
        active BOOLEAN,
        reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )";

    $login = "CREATE TABLE diner.login (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(30) NOT NULL UNIQUE,   
        password VARCHAR(30) NOT NULL
    )";

    $promo = "CREATE TABLE diner.promotion (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        tier INT(6),
        name VARCHAR(30) NOT NULL,
        percent_off INT(6),
        active BOOLEAN,
        entry_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )";

    $menu = "CREATE TABLE diner.menu (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(30),
        category VARCHAR(30),
        price DOUBLE,
        in_stock BOOLEAN,
        promo_off INT(6),
        description VARCHAR(30),
        pic_link TEXT
    )";

    $points = "CREATE TABLE diner.points (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(30),
        points DOUBLE
    )";

    $order = "CREATE TABLE diner.order (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(30),
        order_status VARCHAR(30),
        promo_code VARCHAR(30),
        discount INT(6),
        total DOUBLE,
        comment TEXT,
        create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )";

    $orderItem = "CREATE TABLE diner.order_item(
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        order_id INT(6),
        item_id INT(6),
        item_count INT(6)
    )";

    $message = "CREATE TABLE diner.promo_msg(
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        tier INT(6),
        message_text TEXT
    )";

    $baseline = "CREATE TABLE diner.baseline(
        category VARCHAR(30) PRIMARY KEY,
        level DOUBLE,
        slope DOUBLE,
        seasonality DOUBLE
    )";

    $conn->query($user);
    $conn->query($login);
    $conn->query($promo);
    $conn->query($menu);
    $conn->query($points);
    $conn->query($order);
    $conn->query($orderItem);
    $conn->query($message);
    $conn->query($baseline);
    

    //docker run --name test -d -p 3307:3306 5d30c23e4363
?>
