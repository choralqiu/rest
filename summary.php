<?php 
include 'holtWinters.php';

class Summary { 

    public static function getCountSummary( $interval="10080 MINUTE" ){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "SELECT m.name, sum(oi.item_count) AS count FROM diner.menu AS m LEFT JOIN diner.order_item AS oi ON oi.item_id=m.id LEFT JOIN diner.order AS o ON o.id=oi.order_id WHERE o.create_time>DATE_SUB(now(), INTERVAL 10080 MINUTE) GROUP BY m.id;";
        $queryResult = $conn->query($query);
        $summarys = array();
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $summary['name'] = $row['name'];
		$summary['count'] = $row['count'];
		$summarys[] = $summary;
            }
        }
        return $summarys;
    }

    public static function getTotalSummary($interval="10080 MINUTE"){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "SELECT count(distinct m.name) AS menu_spread, count(distinct o.id) AS order_count, count(distinct o.username) AS distinct_user  FROM diner.menu AS m LEFT JOIN diner.order_item AS oi ON oi.item_id=m.id LEFT JOIN diner.order AS o ON o.id=oi.order_id  WHERE o.create_time>DATE_SUB(now(), INTERVAL 10080 MINUTE);";
        $queryResult = $conn->query($query);
        $summary = array();
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $summary['menu_spread'] = $row['menu_spread'];
                $summary['order_count'] = $row['order_count'];
                $summary['distinct_user'] = $row['distinct_user'];
            }
        }

        $query = "SELECT sum(total) as revenue FROM  diner.order WHERE create_time>DATE_SUB(now(), INTERVAL 10080 MINUTE);";
        $queryResult = $conn->query($query);
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $summary['revenue'] = $row['revenue'];
            }
        }
        return $summary;
    }

    public static function predictionByCategory(){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "SELECT distinct(category) FROM diner.menu";
        $queryResult = $conn->query($query);
        $summarys = array();
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $summary['category'] = $row['category'];
                $summary['number']  = HoltWinters::predict($row['category']);
                $summarys[] = $summary;
            }
        }
        return $summarys;
    }

};
?> 
