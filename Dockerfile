FROM leafney/ubuntu-mysql
RUN apt-get update
RUN apt-get install -y php apache2 libapache2-mod-php php-mcrypt php-mysql vim
COPY rest.php /opt/
COPY init-mysql.php /opt/
COPY init.sh /opt/
COPY login.php /opt/
COPY apache2.conf /etc/apache2/
