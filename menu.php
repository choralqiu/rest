<?php 
class Menu { 

    public static function newItem($name, $category, $price, $promo_off=0, $description, $picLink){
    	$conn = new mysqli("localhost", "root", "mysql", "diner");
    	$query = "INSERT INTO diner.menu (name, category, price, in_stock, promo_off, description, pic_link) VALUES (\"$name\", \"$category\",\"$price\",\"1\", \"$promo_off\", \"$description\", \"$picLink\" )";
    	$conn->query($query);
    }

    public static function deleteItem($id){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "DELETE FROM diner.menu WHERE id=$id ";
        $conn->query($query);
    }
    
    public static function disableItem($id){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "UPDATE diner.menu SET in_stock=0 WHERE id=$id ";
        $conn->query($query);
    }

    public static function enableItem($id){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "UPDATE diner.menu SET in_stock=1 WHERE id=$id ";
        $conn->query($query);
    }

    public static function getAvailableItems( $onlyEnabled ){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "SELECT id, name, category, price, promo_off, description, in_stock, pic_link FROM diner.menu";
        if( $onlyEnabled ){
            $query .= " WHERE in_stock=1";
        }
        $queryResult = $conn->query($query);
        $results = array();
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $result['id'] = $row['id'];
                $result['name'] = $row['name'];
                $result['category'] = $row['category'];
                $result['promo_off'] = $row['promo_off'];
                $result['description'] = $row['description'];
                $result['price'] = $row['price']*(100-$row['promo_off'])/100;
                $result['in_stock'] =  $row['in_stock'];
                $result['pic_link'] = $row['pic_link'];
                $results[] = $result;
            }
        }
        return $results;
    }

    public static function getPopularItems(){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $query = "SELECT item_id, sum(item_count) AS c FROM order_item GROUP BY item_id ORDER BY c DESC LIMIT 3";
        $queryResult = $conn->query($query);
        $ids = array();
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                array_push($ids, $row['item_id'] );
            }
        }
        return self::getItemByIds($ids);
    }

    private function getItemByIds( $ids ){
        $conn = new mysqli("localhost", "root", "mysql", "diner");
        $idString = implode(",", $ids);
        $query = "SELECT id, name, category, price, promo_off, description, in_stock, pic_link FROM diner.menu";
        $query .= " WHERE id in (".$idString." )";

        $queryResult = $conn->query($query);
        $results = array();
        if(mysqli_num_rows($queryResult) > 0){
            while( $row = mysqli_fetch_array($queryResult) ){
                $result['id'] = $row['id'];
                $result['name'] = $row['name'];
                $result['category'] = $row['category'];
                $result['promo_off'] = $row['promo_off'];
                $result['description'] = $row['description'];
                $result['price'] = $row['price']*(100-$row['promo_off'])/100;
                $result['in_stock'] =  $row['in_stock'];
                $result['pic_link'] = $row['pic_link'];
                $results[] = $result;
            }
        }
        return $results;
    }

};
?> 
